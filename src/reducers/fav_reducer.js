import { ADD_FAV, DEL_FAV } from '../actions';

export default function addToFavorite(state = [], action) {
  switch (action.type) {
    case ADD_FAV:
      let favoriteMovies= [...state, action.movie];
      return favoriteMovies;
    case DEL_FAV:
      favoriteMovies = state.filter( (movie) => movie.id !== action.movie.id );
      return favoriteMovies;
    default:
      return state;
  }
}
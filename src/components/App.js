import React, { Component } from 'react';
import MovieResults from "./MovieResults";
import FavoriteMovieList from "./FavoriteMovieList";

import {
  BrowserRouter as Router,
  Switch,
  Route
} from 'react-router-dom';

class App extends Component {
  render() {
    return (
      <div className='container'>
        <div className="row">
          <div className="jumbotron text-center">
            <h1>Movies App</h1>
            <p>Who does not love movies?</p>
          </div>
        </div>
        <div className="row">
          <Router>
            <Switch>
              <Route path="/" component={MovieResults} />
              <Route path="/fav" component={FavoriteMovieList} />
            </Switch>
          </Router>
        </div>
      </div>
    );
  }
}

export default App;

import React, { Component } from 'react';
import { addToFavorite, deleteFromFavorite } from "../actions/index";
import { connect } from 'react-redux';

class MovieItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      favorited: false
    }
  }

  addToFav() {
    this.setState({favorited: !this.state.favorited});
    this.props.addToFavorite(this.props.movie);
  }

  delFromFav() {
    this.setState({favorited: !this.state.favorited});
    this.props.deleteFromFavorite(this.props.movie);

  }
  displayFavorite() {
    if (!this.state.favorited) {
      return <span className="glyphicon glyphicon-heart-empty"
                      onClick={ e => this.addToFav() }
      ></span>;
    } else {
      return <span className="glyphicon glyphicon-heart"
                      onClick={ e => this.delFromFav() }
      ></span>;

    }
  }
  render() {
    const { movie } = this.props;
    return (
        <div className="col-sm-3">
          <div className="thumbnail">
            <img src={`https://image.tmdb.org/t/p/w342/${movie.poster_path}`} alt=""/>
            <div className="caption">
              <h3>{movie.title}</h3>
              <p>{movie.overview}</p>
              <p>Ratings - <span className="badge badge-default">
                <span className="glyphicon glyphicon-star"></span>
                {movie.vote_average}
              </span> </p>
              <p>Release Date - {movie.release_date}</p>
                <p>{this.displayFavorite()}</p>
            </div>
          </div>
        </div>
    );
  }
}

export default connect(null,{addToFavorite, deleteFromFavorite})(MovieItem);
import React, { Component } from 'react';
import Search from './Search';
import { connect } from 'react-redux';
import MovieItem from "./MovieItem";
import { Link } from 'react-router-dom';

class MovieResults extends Component {
  render() {
    return (
      <div>
        <Link to="/fav">Favorite</Link>
        <Search />
        <hr/>
          {this.props.movies.map( movie => (
            <MovieItem movie={movie} key={movie.id}   />
          ))}

      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    movies: state.movies
  }
}
export default connect(mapStateToProps, null)(MovieResults);
import  React, { Component } from 'react';
import { Form, FormControl, FormGroup, ControlLabel, Button } from 'react-bootstrap';
import { API_KEY } from './secrets';
import  { movies } from '../actions';
import { connect } from 'react-redux';

class Search extends Component {
  constructor(props){
    super(props);
    this.state = {
      query: ''
    };

    this.search = this.search.bind(this)
  }

  search(e) {
    e.preventDefault();
    let url = `https://api.themoviedb.org/3/search/movie?api_key=${API_KEY}&page=1&query=${this.state.query}`;
    fetch(url, {
      method: 'GET'
    }).then( res => res.json() )
      .then(jsonObj => this.props.movies(jsonObj.results));
  }
  render() {
    return (
      <div className='row'>
      <Form inline className='col-md-6 col-md-offset-3'>
        <FormGroup>
          <ControlLabel>Search</ControlLabel>
          { ' ' }
          <FormControl
            type='text'
            placeholder='search it'
            onChange={ (event) => this.setState({query: event.target.value })}
          />
          { ' ' }
          <Button
            bsStyle='primary'
            onClick={ e => this.search(e)}
          >
            Submit
          </Button>
        </FormGroup>
      </Form>
      </div>
    )
  }
}

export default connect(null, { movies })(Search);
import React, { Component } from 'react';
import { connect } from 'react-redux';
import MovieItem from "./MovieItem";
import { Link } from 'react-router-dom';

class FavoriteMovieList extends Component {
  render() {
    return (
      <div>
        <h4>My favorite movies</h4>
        <hr/>
        <Link to='/'>Home</Link>
        {this.props.favorites.map( (fav, inx) => (
          <MovieItem key={inx} movie={fav}/>
        ))}

      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    favorites: state.favorites
  }
}

export default connect(mapStateToProps, null)(FavoriteMovieList);
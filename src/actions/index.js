export  const MOVIES = 'MOVIES';
export const ADD_FAV = 'ADD_FAV';
export const DEL_FAV = 'DEL_FAV';

export function movies(items) {
   const action= {
    type: MOVIES,
     items
  };
   return action;
}

export function addToFavorite(movie) {
  return {
    type: ADD_FAV,
    movie
  }
}

export function deleteFromFavorite(movie) {
  return {
    type: DEL_FAV,
    movie
  }

}